#importa bibliotecas que julga ser necessárias
from bs4 import BeautifulSoup
import requests

#cabeçalho caso haja requisições
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'
}

#cria variavel para armazenar classe (requests) com método que faz requisição ao site, com informações adicionais de cabeçalho
req = requests.get('https://quotes.toscrape.com/tag/books/', headers=headers)

#cria variavel para armazenar objeto (beatiful soup) que recebe dados da requisição http anterior.
soup = BeautifulSoup(req.text, 'html.parser')

#cria variavel que ativa o método find_all para a varivael soup, tendo como parâmetros tags "div" com class "quote"
copy = soup.find_all('div', class_='quote')

#cria um for com um objeto qe para cada elemento na variavel copy
for qe in copy:
    
#cria uma variavel para cada dado desejado (texto, autor, categoria), e dentro usa o método find no objeto qe que faz uma busca
#para cada tag com sua respectiva classe desejada, e retorna texto com .text 

    text = qe.find('span', class_='text').text  
    author = qe.find('small', class_='author').text
    
#por "tag" estar dentro da tag "a", que por sua vez está dentro de uma div, cria-se dois finds: o primeiro para encontrar 
#a div onde está as tags, e o segundo para encontrar a tag onde as "tags" se encontram.
    tag = qe.find('div', class_='tags').find('a', class_='tag').text

#um print simples
    print(text+ " ,", author)
    print(tag+"\n")