import csv
from bs4 import BeautifulSoup
import requests

headers = {
    "User-Agent":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/120.0.0.0 Safari/537.36 OPR/106.0.0.0"
}

req = requests.get("https://www.scrapethissite.com/pages/simple/", headers=headers)
soup = BeautifulSoup(req.text, "html.parser")
cript = []

scanner = soup.find_all('div', class_='country')

for a in scanner:
    country = a.find('h3', class_="country-name").text
    country = country.strip()
    capital = a.find('span', class_='country-capital').text
    population = a.find('span', class_='country-population').text
    area = a.find('span', class_='country-area').text
    cript.append(
        {
            'país ': country,
            'capital ': capital,
            'população ': population.strip(),
            'area ': area
        }
)
print(country)

print('succefully')

file_csv = open('dados_paises.csv', 'w', encoding='utf-8', newline='')
writer = csv.writer(file_csv)
writer.writerow(['país', 'capital', 'população', 'area'])

for b in cript:
    writer.writerow(b.values())

file_csv.close()

print ('succefully saved')

