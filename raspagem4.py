import requests
from bs4 import BeautifulSoup

headers = {
    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/120.0.0.0 Safari/537.36 OPR/106.0.0.0'
}

req = requests.get('https://www.saude.am.gov.br/acesso-a-informacao/institucional/', headers=headers)
soup = BeautifulSoup(req.text, 'html.parser')

# Encontrar todos os parágrafos dentro da div 'article-body'
paragrafos = soup.find('div', class_='article-body').find_all('p')

# Filtrar os parágrafos que começam com a palavra "lei"
textos_filtrados = [p.get_text() for p in paragrafos if p.get_text().lower().startswith('lei')]

for texto in textos_filtrados:
    print(texto)
